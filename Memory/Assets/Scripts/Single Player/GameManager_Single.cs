﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameManager_Single : MonoBehaviour
{
    #region Private Variables

    //Card Object
    [SerializeField] private MainCard_Single _Card;
    //List of Card Images Sprites
    [SerializeField] private Sprite[] _CardImages;
    [SerializeField] private GameObject _Canvas;

    //Score labels for the players + Winner Text
    [SerializeField] private TextMesh _Scorelabel;
    [SerializeField] private Text _WinnerText;

    //First Card Turned
    private MainCard_Single _FirstRevealed;
    //Second Card Turned
    private MainCard_Single _SecondRevealed;

    //Score generated when matching
    private int _Score = 0;
    private int _Matches = 4;


    #endregion

    #region Public Variables

    //Amount of card rows
    public const int Rows = 2;
    //Amount of card colums
    public const int Colums = 4;
    //Offset between each card on x and y axis
    public const float OffsetX = 3f;
    public const float OffsetY = 3.5f;

    //Card Container
    public GameObject CardContainerObject;

    #endregion

    private void Start()
    {
        //Start position of the first card object
        float yPosition = -2.9f;
        float xPosition = -4.5f;

        //Start position of the cards
        Vector3 StartPos = new Vector3(xPosition, yPosition, 0);

        int[] numbers = { 0, 0, 1, 1, 2, 2, 3, 3 };
        numbers = ShuffleArray(numbers);

        for (int i = 0; i < Colums; i++)
        {
            for (int j = 0; j < Rows; j++)
            {
                //Instantiate the Card Object
                MainCard_Single Clone = Instantiate(_Card) as MainCard_Single;
                int index = j * Colums + i;
                int id = numbers[index];
                //Sets the Id of the clones including the images that belong to them
                Clone.TurnCard(id, _CardImages[id]);

                /*
                 * Making sure that each clone's position is offset by the amount of [offset variable] * the 
                 * amount of times the game has looped through here then add that amount the start position of 
                 * the first card object.
                */
                xPosition = (OffsetX * i) + StartPos.x;
                yPosition = (OffsetY * j) + StartPos.y;
                Clone.transform.position = new Vector3(xPosition, yPosition, 0);

                //Set each clone as a child under the parent Object CardContainer
                Clone.gameObject.transform.SetParent(CardContainerObject.transform);
            }
        }
    }

    private int[] ShuffleArray(int[] numbers)
    {
        int[] NumArray = numbers.Clone() as int[];
        for (int i = 0; i < NumArray.Length; i++)
        {
            int tmp = NumArray[i];
            int r = Random.Range(i, NumArray.Length);
            NumArray[i] = NumArray[r];
            NumArray[r] = tmp;
        }
        return NumArray;
    }

    public bool CanReveal
    {
        /*
         * Checking if no more than 2 cards are revealed at a time. If it returns true, it means 
         * that the Second Card container is empty, and therefore the player is allowed to turn
         * one more card if the get returns SecondRevealed == null.
         */
        get { return _SecondRevealed == null; }
    }

    public void CardReveal(MainCard_Single Card)
    {
        /*
         * Checking if any cards have been turned at the start of the round. We're keeping track 
         * of 2 containers. If the first one is empty, it means no cards have been turned up until 
         * now. Else, the card that has been turned is the second card and we need to check if 
         * there's a match.
         */
        if (_FirstRevealed == null)
        {
            _FirstRevealed = Card;
        }
        else
        {
            _SecondRevealed = Card;
            StartCoroutine(CheckMatch());
        }

    }

    private IEnumerator CheckMatch()
    {
        /*
         * If the Card ID's of both cards match, then update the score label. If they dont,
         * then turn back both cards and wait for 0.5s. Then, empty the containers (this applies 
         * to both states and therefore is placed outside of the if statement). 
         */
        if (_FirstRevealed.Id == _SecondRevealed.Id)
        {

            _Score++;
            _Scorelabel.text = "Score : " + _Score;
            _Matches -= 1;

            if (_Matches == 0)
            {
                _WinnerText.text = "You have cleared the board!";
                _Canvas.SetActive(true);
            }
        }
        else
        {
            yield return new WaitForSeconds(0.5f);
            _FirstRevealed.Unreveal();
            _SecondRevealed.Unreveal();
        }
        _FirstRevealed = null;
        _SecondRevealed = null;
    }

    public void Restart()
    {
        //Restarts the level
        SceneManager.LoadScene("Level_Single_8");
    }

}
