﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCard_Single : MonoBehaviour
{
    #region Private Variables

    //Back of the Card
    [SerializeField] private GameObject _Back;

    private int _CardId;

    #endregion

    #region Public Variables

    public int Id { get { return _CardId; } }

    #endregion

    #region Other Variables
    // Adding the GameManager object
    GameObject _GameManager;
    #endregion

    private void Awake()
    {
        _GameManager = GameObject.Find("GameManager");
    }

    public void OnMouseDown()
    {
        //If the back of the card is active AND the CanReveal method returns true
        if (_Back.activeSelf && _GameManager.GetComponent<GameManager_Single>().CanReveal)
        {
            //If the back of the card is visible, set the back of the card on inactive
            _Back.SetActive(false);
            //Reveal the card's face side by running the CardReveal method
            _GameManager.GetComponent<GameManager_Single>().CardReveal(this);
        }
    }

    public void TurnCard(int id, Sprite Image)
    {
        //Set the id to the CardId of that card
        _CardId = id;
        //Get the Spriterenderer component and changes the property of its sprite.
        GetComponent<SpriteRenderer>().sprite = Image;
    }

    public void Unreveal()
    {
        //Turn the back of the image back on
        _Back.SetActive(true);
    }
}
