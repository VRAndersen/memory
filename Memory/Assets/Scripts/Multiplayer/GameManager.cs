﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public enum Turns { START, PLAYERTURN, PLAYER2TURN, LEVELOVER }

public class GameManager : MonoBehaviour
{
    #region Private Variables

    //Card Object
    [SerializeField] private MainCard _Card;
    //List of Card Images Sprites
    [SerializeField] private Sprite[] _CardImages;
    [Space]
    [SerializeField] private GameObject _Canvas;

    //Score labels for the players + Winner Text
    [SerializeField] private TextMesh _Scorelabel;
    [SerializeField] private TextMesh _ScorelabelOpponent;
    [SerializeField] private Text _WinnerText;

    //First Card Turned
    private MainCard _FirstRevealed;
    //Second Card Turned
    private MainCard _SecondRevealed;

    //Score generated when matching
    private int _Score = 0;
    private int _Score2 = 0;

    private int _Matches = 4;


    #endregion

    #region Public Variables

    //State of the game
    
    public Turns State;


    //Amount of card rows
    
    public const int Rows = 2;
    
    //Amount of card colums
    public const int Colums = 4;
    //Offset between each card on x and y axis
    public const float OffsetX = 3f;
    public const float OffsetY = 4.5f;

    //Card Container

    public GameObject CardContainerObject;

    #endregion

    private void Start()
    {
        StartCoroutine(SetUpCards());
        //Setting the state to player 1's turn
        State = Turns.PLAYERTURN;      
    }

    IEnumerator SetUpCards()
    {
        //Setting the state to the start of the game
        State = Turns.START;

        //Start position of the first card object
        float yPosition = -2.9f;
        float xPosition = -4.5f;

        //Start position of the cards
        Vector3 StartPos = new Vector3(xPosition, yPosition, 0);

        int[] numbers = { 0, 0, 1, 1, 2, 2, 3, 3 };
        numbers = ShuffleArray(numbers);

        for (int i = 0; i < Colums; i++)
        {
            for (int j = 0; j < Rows; j++)
            {
                //Instantiate the Card Object
                MainCard Clone = Instantiate(_Card) as MainCard;
                int index = j * Colums + i;
                int id = numbers[index];
                //Sets the Id of the clones including the images that belong to them
                Clone.TurnCard(id, _CardImages[id]);

                /*
                 * Making sure that each clone's position is offset by the amount of [offset variable] * the 
                 * amount of times the game has looped through here then add that amount the start position of 
                 * the first card object.
                */
                xPosition = (OffsetX * i) + StartPos.x;
                yPosition = (OffsetY * j) + StartPos.y;
                Clone.transform.position = new Vector3(xPosition, yPosition, 0);

                //Set each clone as a child under the parent Object CardContainer
                Clone.gameObject.transform.SetParent(CardContainerObject.transform);
            }
        }
        yield return new WaitForSeconds(2f);
    }

    private int[] ShuffleArray(int[] numbers)
    {
        int[] NumArray = numbers.Clone() as int[];
        for (int i = 0; i < NumArray.Length; i++)
        {
            int tmp = NumArray[i];
            int r = Random.Range(i, NumArray.Length);
            NumArray[i] = NumArray[r];
            NumArray[r] = tmp;
        }
        return NumArray;
    }

    public bool CanReveal
    {
        /*
         * Checking if no more than 2 cards are revealed at a time. If it returns true, it means 
         * that the Second Card container is empty, and therefore the player is allowed to turn
         * one more card if the get returns SecondRevealed == null.
         */
        get { return _SecondRevealed == null; }
    }

    public void CardReveal(MainCard Card)
    {
        /*
         * Checking if any cards have been turned at the start of the round. We're keeping track 
         * of 2 containers. If the first one is empty, it means no cards have been turned up until 
         * now. Else, the card that has been turned is the second card and we need to check if 
         * there's a match.
         */
        if (_FirstRevealed == null)
        {
            _FirstRevealed = Card;
        }
        else
        {
            _SecondRevealed = Card;
            StartCoroutine(CheckMatch());
        }

    }

    private IEnumerator CheckMatch()
    {
        /*
         * If the Card ID's of both cards match, then update the score label. If they dont,
         * then turn back both cards and wait for 0.5s. Then, empty the containers (this applies 
         * to both states and therefore is placed outside of the if statement). 
         */
        if (_FirstRevealed.Id == _SecondRevealed.Id)
        {
            //Checking which player turn it is
            if (State == Turns.PLAYERTURN)
            {
                //Change their label respectively (player 1)
                _Score++;
                _Scorelabel.text = "Score : " + _Score;
                State = Turns.PLAYERTURN;

            }
            else
            {
                //Change their label respectively (player 2)
                _Score2++;
                _ScorelabelOpponent.text = "Score : " + _Score2;
                State = Turns.PLAYER2TURN;
            }

            _Matches -= 1;

            if (_Matches == 0)
            {
                if (_Score > _Score2)
                {
                    _WinnerText.text = "Player 1 Won! ";
                    State = Turns.LEVELOVER;
                }
                else
                {
                    _WinnerText.text = "Player 2 Won! ";
                    State = Turns.LEVELOVER;
                }
                _Canvas.SetActive(true);
            }
        }
        else
        {
            yield return new WaitForSeconds(0.5f);
            _FirstRevealed.Unreveal();
            _SecondRevealed.Unreveal();

            //Checking which player turn it is
            if (State == Turns.PLAYERTURN)
            {
                //If it was player 1's turn, it's player 2's turn now
                State = Turns.PLAYER2TURN;
            }
            else
            {
                //It was player 2's turn, so it's player 1's turn now.
                State = Turns.PLAYERTURN;
            }
        }
        _FirstRevealed = null;
        _SecondRevealed = null;
    }

    public void Restart()
    {
        //Restarts the level
        SceneManager.LoadScene("Level_Double_8");
    }
}
